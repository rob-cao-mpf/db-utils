import os
import pandas as pd
import vertica_python

from jinjasql import JinjaSql

ip_addr = {'NA_PROD':'10.21.0.133',
           'SANDBOX':'10.21.116.20',
           'NA_QA':'10.17.52.10',
           'DS':'10.21.116.19'}
db = {'NA_PROD':'ODSWPROD',
      'NA_QA':'ODSDWQA',
      'SANDBOX':'',
      'DS':'EODSDMSB'}


### Create database connector
def connect(host,database=None,user=None,password=None):
    ip = ip_addr.get(host)
    db_info = {'host': ip if ip else host,
               'database': db.get(host) if ip else database,
               'user': os.getenv(f'{host}_ID') if ip else user, 
               'password': os.getenv(f'{host}_PWD') if ip else password,
               'port': 5433, 
               'read_timeout': 600, 
               'unicode_error': 'strict', 
               'autocommit': True,
               'ssl': False}
    return vertica_python.connect(**db_info)

### Execute SQL command
def read(host,sql,params={}):
#def read(conn_info,sql,params={}):
    
    ## If sql is a template
    if os.path.exists(sql):
        j = JinjaSql(param_style='pyformat')
        with open(sql) as f:
            template = f.read()  
            sql, bind_params = j.prepare_query(template,params)
            bind_params = reformat_strings(bind_params)
    
    with connect(host) as conn:
        sql = sql % bind_params if params else sql
        df = pd.read_sql(sql,conn)
    return df


def reformat_strings(query_params):
    return {key:f"'{val}'" 
            if isinstance(val,str) else val 
            for key,val in query_params.items()}


### Write to table directly from dataframe
def write(data,host,schema,table):
    typemap = {'bool': 'boolean',
               'float64': 'numeric(37,15)',
               'int64': 'int',
               'object':'varchar(80)'}
    
    ## Column types
    colnames = data.columns.tolist()
    coltypes = [typemap.get(t.name) for t in data.dtypes]
    columns = ','.join([' '.join([a,b]) for a,b in zip(colnames,coltypes)])
    
    with connect(host).cursor() as cur:
        ## Create
        try:
            create = f"CREATE TABLE {schema}.{table} ({columns});"
            cur.execute(create)
            print(f'New table {schema}.{table} created in {host}')
        except vertica_python.errors.DuplicateObject:
            pass
        
        ## Copy
        copy = f"COPY {schema}.{table} ({','.join(colnames)}) from stdin DELIMITER '*'"
        cur.copy(copy,data.to_csv(header=None, index=False,sep='*'))
        
        
    